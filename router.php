<?
//error_reporting(E_ALL); 
//ini_set("display_errors", 1);
    header('Content-Type: application/json;filename="file.json');
    require("functions.php");
    include_once('../common/include.php');
    include_once('../common/encipher.php');
    ini_set("allow_url_fopen", true);
    ini_set('date.timezone', 'Asia/Jakarta');
    date_default_timezone_set('Asia/Jakarta');
    $starttime = microtime(true);
    $conn=getConnection();


    $req = geturi();



    $version = $req[2]; // Version
    $mod = $req[3]; // Module   
    $user = $req[4]; // User ID
    $token = $req[5]; // Token

    if (substr($version,0,1) !== "v"){
        $version = "v1";
        $mod = $req[2];
    }


    $jsondata = json_decode(file_get_contents("php://input"));
 
if($conn==null){
        sendResponse(500, $conn, 'Server DB Connection Error !');
} else {
    //============ check token start
    
    
    
    
    if (!auth_check($conn,$user,$token,$mod)) {
        sendResponse(401,$conn,'Anda telah login ditempat lain');
    } else {

    //============ check token end

        //============ check mandatory start
        $mdt = mandatory_check($conn,$jsondata,$mod);
        if (!$mdt["sts"]){
             sendResponse(400,$jsondata  , $mdt["remarks"]); 
        } else {
        //============ check mandatory end

            if (file_exists($version."/".$mod.".php")){
                if($conn==null){
                    sendResponse(500,$conn,'Server Connection Error');
                }else{
                    //$userlog = user_check($conn,$uid,$token);
                    $valid = true;
                   
                        
                    if ($valid){
                     include($version."/".$mod.".php");
                    } else {
                        sendResponse(404,$jsondata  , 'Invalid User, Please Re-Login'); 
                    }
                }

            } else {
                sendResponse(500,$conn,'Method Not Found');
            }
        }
    }
    $endtime = microtime(true);
    $timediff = $endtime - $starttime;
   
     $conn->close();
}
?>
