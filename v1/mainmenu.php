<?
    $code = 200;
    $msg = "";

    // path induk

    $version = $conn->real_escape_string($req[2]); // Version
    $mod = $conn->real_escape_string($req[3]); // Module  
    $uid = $conn->real_escape_string($req[4]); // User ID
    $token = $conn->real_escape_string($req[5]);
    $cari = strtolower($conn->real_escape_string($req[8]));

    // path main menu

    $perusahaan = $req[6];
    $periode = $req[7];

    $rasio = array();
    $karyawaninti = array();
    $karyawanusia = array();
    $karyawangender = array();
    $karyawanpendidikan = array();
    $karyawanmilenial = array();
    $karyawanpendidikan = array();
    $karyawandirektorat = array();
    $organisasi = array();
    $level = array();
    $levelorganisasi = array();
    $layerorganisasi = array();
    $talent = array();
    $struktur = array();
    $pengalaman = array();
    $penghargaan = array();
    $pendidikanhist = array();
    $sphist = array();
    $matrixorganisasi = array();
    $issp = false;

    $dir = "";
    $wherep = "1=1";
    if ($perusahaan == "99"){
        $dir = "dir_";
        
    } else {
        $wherep = "idperusahaan = '$perusahaan'";
    }
    

    $skip = array("idperusahaan", "bulan", "tahun","kodestatusdetail");

    if ($perusahaan=="99"){
        $sql = "SELECT
	tahun,
	bulan,
	Sum(INTI)INTI ,
	sum(PKWT) PKWT,
	sum(PenugasankeKSGroup) PenugasankeKSGroup,
	sum(PenugasandariKSGroup ) PenugasandariKSGroup
FROM
	totalkaryawan 
    where tahun = '".substr($periode,0,4)."'  and bulan = '".substr($periode,4,2)."'
GROUP BY
	tahun,
	bulan
	 ";
    } else {

       $sql = "select * from ".$dir."totalkaryawan where $wherep  and tahun = '".substr($periode,0,4)."'  and bulan = '".substr($periode,4,2)."' ";  
    }


     $mainsql = $sql;
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            
           
            
            $total = 0;
            while($row = $result->fetch_assoc()) {
                  
                $columns = array_keys($row);
                foreach ($columns as $value) {
                    if (in_array($value, $skip)) continue;
                        
                  $zz["field"] = $value;
                  $zz["field"] = str_replace("PenugasankeKSGroup","Penugasan Ke \nKrakatau Steel Group",$zz["field"]);
                  $zz["field"] = str_replace("PenugasandariKSGroup","Penugasan dari \nKrakatau Steel Group",$zz["field"]);
                  $zz["val"]  =   $row[$value];
                  $total = $total + intval($row[$value]);
                   array_push($rasio,$zz);   
                }
                
                //print_r($columns) ;
                //$r=$row;
                //$r = transpose($r);
                //array_push($rasio,$r);
            }
        
            //convert to percent
            
            foreach ($rasio as $key => $value) {
                $rasio[$key]["pct"] = (String)(round($rasio[$key]["val"] / $total * 100,0));
                 if (intval($rasio[$key]["val"]) == 0) $rasio[$key]["pct"] = "0";
            }
            
        
        /*
        usort($rasio, function($a, $b) {
            return $a['val'] <=> $b['va;'];
        });
        */
        
        
        
        
    } 
    //print_r($rz);
/*
    $sql = "select * from karyawaninti where idperusahaan = '$perusahaan'";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            while($row = $result->fetch_assoc()) {
            
                $columns = array_keys($row);
                foreach ($columns as $value) {
                    if (in_array($value, $skip)) continue;
                  $zz["field"] = $value;
                  $zz["val"]  =   $row[$value];
                    $zz["field"] = str_replace("PenugasankeKSGroup","Penugasan Ke \nKrakatau Steel Group",$zz["field"]);
                  $zz["field"] = str_replace("PenugasandariKSGroup","Penugasan dari \nKrakatau Steel Group",$zz["field"]);
                  $zz["field"] = str_replace("(","",$zz["field"]);
                  $zz["field"] = str_replace(")","",$zz["field"]);
                   array_push($karyawaninti,$zz);   
                }
            }
    } 

*/
    $sql = "select * from  ".$dir."karyawaninti where $wherep  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."'";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            $total = 0;
            while($row = $result->fetch_assoc()) {
 
                 $rw["field"] = $row["statusdetail"];
                 //$rw["fieldori"] = $row["statusdetail"];
                 $rw["field"] = str_replace("(","\n",$rw["field"]);
                 $rw["field"] = str_replace("  "," ",$rw["field"]);
                 $rw["field"] = str_replace("\n ","\n",$rw["field"]);
                 $rw["field"] = str_replace(")","",$rw["field"]);
                 $rw["field"] = str_replace("Berkepanjangan","\nBerkepanjangan",$rw["field"]);
                
                $rw["val"] = $row["jumlah"];
                $total = $total + intval($row["jumlah"]);
                array_push($karyawaninti,$rw); 
            }
        //echo $total;
        foreach ($karyawaninti as $key => $value) {
                //print_r($karyawaninti[$key]);
                //echo $total;
                
                $karyawaninti[$key]["pct"] = (String)(round($karyawaninti[$key]["val"] / $total * 100,0));
                if (intval($karyawaninti[$key]["val"]) == 0) $karyawaninti[$key]["pct"] = "0";
            }
    } 

    $sql = "select * from  ".$dir."usia where $wherep  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."'";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            while($row = $result->fetch_assoc()) {
 
                 $rw["field"] = $row["namausia"];
                $rw["val"] = $row["jumlah"];
                array_push($karyawanusia,$rw); 
            }
    } 

    $sql = "select * from  ".$dir."jeniskelamin where $wherep  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."'";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            $total = 0;
            while($row = $result->fetch_assoc()) {
 
         
                $rw["field"] = $row["namajeniskelamin"];
                $rw["val"] = $row["jumlah"];
                $total = $total + intval($row["jumlah"]);
                array_push($karyawangender,$rw); 
            }
        
        foreach ($karyawangender as $key => $value) {
                $karyawangender[$key]["pct"] = (String)(round($karyawangender[$key]["val"] / $total * 100,0));
                if (intval($karyawangender[$key]["val"]) == 0) $karyawangender[$key]["pct"] = "0";
            }
    } 

    $sql = "select * from  ".$dir."pendidikan where $wherep  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."'  order by kodependidikan desc";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            $total = 0;
            $undsma = 0;
            while($row = $result->fetch_assoc()) {
 
 
                $rw["field"] = $row["namapendidikan"];
                $rw["field"] = str_replace("/ Sederajat","",$rw["field"]);
                $rw["val"] = $row["jumlah"];
                $total = $total + intval($row["jumlah"]);
                if ($row["kodependidikan"]>2){
                    array_push($karyawanpendidikan,$rw); 
                } else{
                    $undsma += $row["jumlah"];
                }
            }
        
         $rw["field"] = "Dibawah SLTA";
         $rw["val"] = (String)$undsma;
         array_push($karyawanpendidikan,$rw); 
        
        foreach ($karyawanpendidikan as $key => $value) {
                $karyawanpendidikan[$key]["pct"] = (String)(round($karyawanpendidikan[$key]["val"] / $total * 100,0));
                if (intval($karyawanpendidikan[$key]["val"]) == 0) $karyawanpendidikan[$key]["pct"] = "0";
            }
    } 


    if ($perusahaan !== "99"){
     $sql = "select * from  ".$dir."direktorat where $wherep  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."'";    
    if (!$result = $conn->query($sql))
    {  
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            while($row = $result->fetch_assoc()) {
             
                $rw["field"] = $row["namadirektorat"];
                if (strlen($row["namadirektorat"]) > 20){
                   // $rw["field"] = substr($row["namadirektorat"],0,20).
                //        "\n".substr($row["namadirektorat"],20,20);
                    $rw["field"] = str_replace("Direktorat","Direktorat\n",$row["namadirektorat"]);
                }
                $rw["val"] = $row["jumlah"];
                $rw["pct"] = "0";
                 array_push($karyawandirektorat,$rw); 
            }
    }
    }
    /*
    usort($karyawandirektorat, function($a, $b) {
            return $a['val'] <=> $b['val'];
        });
    */
    
// level masih salah, harusnya keluar data golongan ABCDE
$sql = "select * from  ".$dir."leveljabatan where $wherep  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."' and golongan <> 'Z'";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            while($row = $result->fetch_assoc()) {
             
                   $rw["field"] = (String)($row["golongan"]);
                $rw["val"] = $row["jumlah"];
                 array_push($level,$rw); 
            }
    } 

/*
$sql = "select * from leveljabatan where idperusahaan = '$perusahaan'  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."' and golongan <> 'Z'";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            $ke = 1;
            while($row = $result->fetch_assoc()) {
                ///if ($row["golongan"]=="A" && $row["jumlah"] ==0) $minus = 2;
                if ($row["jumlah"]==0){
                    continue;
                }
                $rw["field"] = $row["namalevel"];
                $rw["val"] = "BOD-".$ke;
                $ke++;
                 array_push($organisasi,$rw); 
            }
    } 
*
function aTOn($string) {
     $string = strtoupper($string);
     $length = strlen($string);
     $number = 0;
     $level = 1;
     while ($length >= $level ) {
         $char = $string[$length - $level];
         $c = ord($char) - 64;        
         $number += $c * (26 ** ($level-1));
        $level++;
     }
    return $number;
 }

/*
$sql = "select * from leveljabatan where idperusahaan = '$perusahaan'  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."' and golongan <> 'Z'";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            $ke = 1;
            while($row = $result->fetch_assoc()) {
                if ($row["jumlah"]==0){
                    continue;
                }
                $rw["field"] = "BOD-".$ke;
                $rw["val"] = $row["jumlah"];
                $ke++;
                array_push($levelorganisasi,$rw); 
            }
    } 
*/
 

$sql = "select * from  ".$dir."levelorganisasi where $wherep  and tahun ='".substr($periode,0,4)."' and bulan = '".substr($periode,4,2)."'   order by  kodebod asc";    
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
        
            $raw = array();
        
        
            $maxni= 0;
            $ke = 1;
            while($row = $result->fetch_assoc()) {
                
                $raw[$row["kodebod"]]["nama"] = $row["namabod"];
                $raw[$row["kodebod"]]["namabod"] = $row["namaboddropdown"];
                $raw[$row["kodebod"]]["cs".$row["kodecs"]] = $row["total"];
                
                
                /*
             
                $rw["field"] = "BOD-".$ke;
                $rw["val"] = $row["jumlah"];
                
                $rw["val2"] = (String)rand(0,$row["jumlah"]) ; // (string)intval($row["jumlah"]/4); /// <------------ dummy data
                if ($rw["val"]==0 && $rw["val2"]==0){
                    continue;
                }
                $ke++;
                array_push($layerorganisasi,$rw); 
                
                if ($row["jumlah"] > $maxni) $maxni=$row["jumlah"];
                */
            }
        
            foreach($raw as $r){
                $rw["field"] = str_replace("BOD - ","BOD-",$r["nama"]);
                $rw["val"] = (String)intval($r["cs1"]);
                $rw["val2"] = (String)intval($r["cs2"]);
                array_push($layerorganisasi,$rw); 
                if ($rw["val"] > $maxni) $maxni=$rw["val"];
                if ($rw["val2"] > $maxni) $maxni=$rw["val2"];
                
                $lev = array();
                $lev["field"] = $rw["field"];
                $lev["val"] = (String)($rw["val"] + $rw["val2"]);
                array_push($levelorganisasi,$lev); 
                
                
                ///print_r($r);
                
                $des = array();
                $des["field"] = $rw["field"];
                $des["val"] = (String)($r["namabod"]);
                array_push($organisasi,$des); 
                
            }
        //print_r($raw);
    } 

 

  
    if($perusahaan == "99"){
        $sql = "select sum(genz) genz, sum(millenials) millenials, sum(genx) genx, sum(cnt) cnt
from  generasi_vd ";
    } else {
        $sql = "select * from  generasi_vd where $wherep";
    }
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            while($row = $result->fetch_assoc()) {
            $rw = array(); 
            $rw["field"] = "Generation Z";
            $rw["val"] =  round(($row["genz"] / $row["cnt"]) * 100,0);
            $rw["val"] = (String) $rw["val"] ;
            $rw["cntx"] = (String) $row["cnt"] ;
            $rw["pct"] = "0";
            array_push($karyawanmilenial,$rw); 
            $rw["field"] = "Millenials";
            $rw["val"] =   round(($row["millenials"] / $row["cnt"]) * 100,0);
            $rw["val"] = (String) $rw["val"] ;
            $rw["cntx"] = (String) $row["cnt"] ;
            $rw["pct"] = "0";
            array_push($karyawanmilenial,$rw); 
            $rw["field"] = "Generation X";
            $rw["val"] =   round(($row["genx"] / $row["cnt"]) * 100,0);
            $rw["val"] = (String) $rw["val"] ;
            $rw["cntx"] = (String) $row["cnt"] ;
            $rw["pct"] = "0";
            array_push($karyawanmilenial,$rw); 
            }
    } 

    $where  = "";
    if ($cari){
        $where = " and lower(nama) like  '%$cari%' ";
    }
    $sql = "select *, YEAR(now()) - YEAR(tanggalbekerja) as masakerja,(YEAR(DATE_ADD(tanggallahir,INTERVAL 55 YEAR)) - YEAR(NOW())) sisa  from talent_vd where $wherep  $where ";
    if (!$result = $conn->query($sql))
    {
        $code = 500;
        $msg = ' internal error '.$conn -> error." | ".$sql;
    }
    if ($result->num_rows > 0) {
           // $users=array();
            while($row = $result->fetch_assoc()) {
             
                $pengalaman = array();
                $penghargaan = array();
                $pendidikanhist = array();
                $sphist = array();
                $traininghist = array();
                $talentdet = array();
                $rw = array();
                
                 
                $sql = "SELECT
                    t_registrasi_detail_riwayat.*,
                    m_perusahaan.namaperusahaan,
                    ifnull(YEAR(daritanggal),'') tahun
                    FROM
                    t_registrasi_detail_riwayat
                    JOIN m_perusahaan
                    ON t_registrasi_detail_riwayat.idperusahaan = m_perusahaan.idperusahaan where t_registrasi_detail_riwayat.idperusahaan = '".$row["idperusahaan"]."' and nik = '".$row["nik"]."' and idregistrasi = '".$row["idregistrasi"]."'";
                    if (!$resultz = $conn->query($sql))
                    {
                        $code = 500; 
                        $msg = ' internal error '.$conn -> error." | ".$sql;
                    }
                    if ($resultz->num_rows > 0) {
                           // $users=array();
                            while($rowz = $resultz->fetch_assoc()) {
                                $pengl = array();
                                $pengl["jabatan"] =   $rowz["riwayatjabatan"] ?? "-";
                                $pengl["perusahaan"] =  $rowz["namaperusahaan"] ?? "-";
                                $pengl["tahun"] =    $rowz["tahun"] ?? "-";
                                array_push($pengalaman,$pengl);
                            }
                    } 
                
                $sql = "select *, YEAR(daritanggal)   tahun from t_registrasi_detail_training where $wherep  and nik = '".$row["nik"]."' and idregistrasi = '".$row["idregistrasi"]."'";
                    if (!$resultz = $conn->query($sql))
                    {
                        $code = 500;
                        $msg = ' internal error '.$conn -> error." | ".$sql;
                    }
                    if ($resultz->num_rows > 0) {
                           // $users=array();
                            while($rowz = $resultz->fetch_assoc()) {
                                $pengl = array();
                                $pengl["nama"] =   $rowz["namatraining"] ?? "-";
                                $pengl["tahun"] =   $rowz["tahun"] ?? "-";
                                array_push($traininghist,$pengl);
                            }
                    } 
                
                $sql = "select *, YEAR(daritanggal)   tahun from t_registrasi_sp where $wherep  and nik = '".$row["nik"]."' and idregistrasi = '".$row["idregistrasi"]."'";
                    if (!$resultz = $conn->query($sql))
                    {
                        $code = 500;
                        $msg = ' internal error '.$conn -> error." | ".$sql;
                    }
                    if ($resultz->num_rows > 0) {
                           // $users=array();
                            while($rowz = $resultz->fetch_assoc()) {
                                $pengl = array();
                                $pengl["spke"] = (String) $rowz["idsp"];
                                $pengl["keterangan"] = ($rowz["idketerangan"]==1) ? "Aktif" : "History";
                                $pengl["tahun"] = $rowz["tahun"] ?? "-";
                                if ($rowz["idketerangan"]==1){
                                    $issp = true;
                                }
                                array_push($sphist,$pengl);
                            }
                    } 
                
                
                   $sql = "select *, YEAR(daritanggal)   tahun from t_registrasi_detail_penghargaan where $wherep  and nik = '".$row["nik"]."' and idregistrasi = '".$row["idregistrasi"]."'";
                    if (!$resultz = $conn->query($sql))
                    {
                        $code = 500;
                        $msg = ' internal error '.$conn -> error." | ".$sql;
                    }
                    if ($resultz->num_rows > 0) {
                           // $users=array();
                            while($rowz = $resultz->fetch_assoc()) {
                                $pengl = array();
                                $pengl["nama"] = (String) $rowz["penghargaan"];
                                $pengl["oleh"] = "-";
                                $pengl["tahun"] = $rowz["tahun"] ?? "-";
                                array_push($penghargaan,$pengl);
                            }
                    }
                
                
                $sql = "SELECT
                        m_pendidikan.namapendidikan,
                        t_registrasi_detail.namasekolah,
                        t_registrasi_detail.tahunlulus,
                        t_registrasi_detail.jurusan
                        
                        FROM
                        m_pendidikan
                        JOIN t_registrasi_detail
                        ON m_pendidikan.kodependidikan = t_registrasi_detail.idpendidikan
                        where $wherep  and nik = '".$row["nik"]."' and idregistrasi = '".$row["idregistrasi"]."'";
                    if (!$resultz = $conn->query($sql))
                    {
                        $code = 500;
                        $msg = ' internal error '.$conn -> error." | ".$sql;
                    }
                    if ($resultz->num_rows > 0) {
                           // $users=array();
                            while($rowz = $resultz->fetch_assoc()) {
                                $pendh = array();
                                $pendh["jenjang"] = $rowz["namapendidikan"] ?? "";
                                $pendh["nama"] = $rowz["namasekolah"] ?? "";
                                $pendh["jurusan"] = $rowz["jurusan"] ?? "";
                                $pendh["tahun"] = $rowz["tahunlulus"] ?? "";
                                $pendh["jenjang"] = str_replace("/ Sederajat","",$pendh["jenjang"]);
                                array_push($pendidikanhist,$pendh); 
                            }
                    } 
                 
                adddet($talentdet,$row["namastatus"],"Status");
                adddet($talentdet,$row["namalevel"],"Level");
                adddet($talentdet,$row["namaposisi"],"Posisi");
                //adddet($talentdet,$row["namajabatan"],"Jabatan");
                adddet($talentdet,($row["jeniskelamin"] =="L") ? "Pria " : "Wanita","Jenis Kelamin");
                adddet($talentdet,$row["nikktp"],"KTP");
                adddet($talentdet,$row["masakerja"]." thn","Masa Kerja");
                adddet($talentdet,$row["sisa"]." thn","Sisa Masa Kerja");
                adddet($talentdet,$row["namaagama"],"Agama");
                adddet($talentdet,$row["golongandarah"],"Gol. Darah");
                //adddet($talentdet,$row["alamat"],"Alamat");
                
                $rw["talentdet"] = $talentdet;
                $rw["pengalaman"] = $pengalaman;
                $rw["penghargaan"] = $penghargaan;
                $rw["pendidikanhist"] = $pendidikanhist;
                $rw["traininghist"] = $traininghist;
                $rw["sphist"] = $sphist;
                $rw["nik"] = $row["nik"];
                $rw["image"] =  (strlen($row["path"])  > 0) ? $row["path"] : $row["filename"];
                //$rw["image"] =   "https://rnd.krakatau-it.co.id/admin.png";
                $rw["name"] = $row["nama"] ;
                preg_match_all('/[A-Z]/', $row["namaperusahaan"] , $matches, PREG_OFFSET_CAPTURE);
                //$im = implode("",$matches);
                $nm = "";
                foreach ($matches as $value) {
                            foreach ($value as $val) {
                               $nm .= $val[0];
                                 
                            }
                        }
                $rw["namaperusahaan"] = $nm ;
                //$rw["path"] = $row["path"] ; 
                //$rw["filename"] = $row["filename"] ;
                $rw["umur"] = $row["umur"] ;
                $rw["jabatan"] = $row["namajabatan"] ?? "";;
                $rw["level"] =  $row["namakategori"]." ".$nm ?? "";;
                $rw["unitkerja"] = "";
                $rw["status"] = $row["idstatus"] ?? "";;
                $rw["masakerja"] = $row["masakerja"] ?? "";;
                $rw["sisakerja"] =   $row["sisa"];
                $rw["medali"] = (string)count($penghargaan);
                $rw["sp"] = $issp;
                $rw["notelepon"] = $row["notelepon"] ?? "";
                if (substr($rw["notelepon"],0,1) == "0"){
                    $rw["notelepon"] = "+62".substr($rw["notelepon"],1,strlen($rw["notelepon"]));
                }
                $rw["email"] = "";
                $rw["pdf"] = "https://app.krakatausteel.com/demografi/modules/pdf/cetak.php?idper=".$row['idperusahaan']."&nik=". $row["nik"]."&idp=".$row['idregistrasi'];
                $rw["whatsapp"] = $rw["notelepon"] ?? "";;
                
                 
                array_push($talent,$rw); 
                
            }
    } 
    
    function adddet(&$det,$var,$label)
    {
        $pendh = array();
        $pendh["field"] = $label;
        $pendh["val"] = $var ?? "";
        array_push($det,$pendh); 
    }
    
    
  
    $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    $sql = "INSERT INTO `demografi`.`sysmobilelogs`(`ip`, `url`, `browser`) VALUES ('".$_SERVER['REMOTE_ADDR']."', '".$actual_link."', '".$_SERVER['HTTP_USER_AGENT']."')";


    $numlength = strlen((string)$maxni);
    $v = "1";
    for ($i=1;$i<$numlength;$i++){
        $v .= "0";
    }
    ///echo $v;
      $z = intval($v);


    $ret=array(
                "Apps" =>  'DemografiHCM',
                "Route" =>  'api | demografi | token | '.$perusahaan.' | '.$periode,
                "rasio" => $rasio,
                "karyawaninti" => $karyawaninti,
                "karyawanusia" => $karyawanusia,
                "karyawangender" => $karyawangender,
                "karyawanpendidikan" => $karyawanpendidikan,
                "karyawanmilenial" => $karyawanmilenial,
                "karyawandirektorat" => $karyawandirektorat,
                "organisasi" => ($perusahaan == "99") ? array() : $organisasi,
                "level" => $level,
                "levelorganisasi" => $levelorganisasi,
                "layerorganisasi" => $layerorganisasi,
                "talent" => $talent,
                "struktur" => $struktur,
                "matrixorganisasi" => $matrixorganisasi,
                "maxlayerorg" => (String)(ceil(($maxni*2)/$z)*$z),
                 
                "sql" => $sql,
                "mainsql" => $mainsql,
            );

    sendResponse($code,$ret,$msg);

function firstUC ( $subject ) {
  $n = preg_match( '/[A-Z]/', $subject, $matches, PREG_OFFSET_CAPTURE );
  return $n ? $matches[0] : false;
}


    

    
    $conn->query($sql);  

    function transpose($array) {
        return array_map(null, ...$array);
    }

     
?>